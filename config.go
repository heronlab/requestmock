package requestmock

import "gopkg.in/yaml.v3"

type StaticResponse struct {
	Code    int               `yaml:"code"`
	Body    string            `yaml:"body"`
	Headers map[string]string `yaml:"headers"`
}

type Response struct {
	Static *StaticResponse `yaml:"static"`
}

type Route struct {
	Method   string    `yaml:"method"`
	Path     string    `yaml:"path"`
	Delay    int       `yaml:"delay_in_ms"`
	Response *Response `yaml:"response"`
}

type StorageSettings struct {
	Workers int    `yaml:"workers"`
	Type    string `yaml:"type"`
	// extra settings will be added later
}

type Settings struct {
	Routes  []*Route         `yaml:"routes"`
	Port    int              `yaml:"port"`
	Storage *StorageSettings `yaml:"storage"`
}

func NewSettings(yamlContent []byte) (*Settings, error) {
	cfg := &Settings{}
	if err := yaml.Unmarshal(yamlContent, &cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}
