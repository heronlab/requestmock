package requestmock

import (
	"fmt"
	"strings"
)

const (
	stdoutStorageType = "stdout"
)

type Storage interface {
	Store(r *storeItem) error
}

type storeItem struct {
	out string
}

func storageFactory(st *StorageSettings) (Storage, error) {
	switch st.Type {
	case stdoutStorageType:
		return &stdoutStorage{}, nil
	}

	return nil, fmt.Errorf("unknown storage type: %s", st.Type)
}

type stdoutStorage struct {
}

func (s *stdoutStorage) Store(item *storeItem) error {

	// add 2 space before each line to get combined to 1 single message
	out := strings.ReplaceAll(item.out, "\r\n", "\r\n  ")
	fmt.Println(out)

	return nil
}
