# requestmock

A simple HTTP server, that receives, stores, responds requests for debugging purpose. 
You can define what routes to be handled & what they respond via yaml configuration.

```yaml
routes:
  - path: /hello
    method: GET
    response:
      static:
        code: 200
        body: >-
        {"code": "ok"}
        headers:
          content-type: applicaton/json
    delay_in_ms: 0
```
