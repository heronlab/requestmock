package requestmock

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func makeHandler(route *Route) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if route.Response.Static == nil {
			ctx.String(http.StatusNotFound, "unknown response config")
		}

		ctx.Status(route.Response.Static.Code)
		for k, v := range route.Response.Static.Headers {
			ctx.Header(k, v)
		}
		_, _ = ctx.Writer.WriteString(route.Response.Static.Body)

		if route.Delay > 0 {
			<-time.After(time.Millisecond * time.Duration(route.Delay))
		}
	}
}
