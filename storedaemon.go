package requestmock

import (
	"github.com/rs/zerolog/log"
)

func (s *Service) startWorker() {
	for {
		item, ok := <-s.storageInput
		// closed channel
		if !ok {
			return
		}

		err := s.storage.Store(item)
		if err != nil {
			log.Error().Msg(err.Error())
		}
	}
}
