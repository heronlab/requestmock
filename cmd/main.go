package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/htphuongse/requestmock"
	"io"
	"os"

	"github.com/rs/zerolog"
)

var (
	configPath = "config.yml"
	debug      = false
)

func main() {
	flag.StringVar(&configPath, "config", configPath, "path to config file")
	flag.BoolVar(&debug, "debug", debug, "set log level to debug")
	flag.Parse()

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	if debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	content, err := readFile(configPath)
	if err != nil {
		log.Error().Msg(err.Error())
		os.Exit(1)
	}

	st, err := requestmock.NewSettings(content)
	if err != nil {
		log.Error().Msg(err.Error())
		os.Exit(1)
	}

	service := requestmock.NewService(st)
	_ = service.Start(context.Background())
}

func readFile(p string) ([]byte, error) {
	fh, err := os.Open(p)
	if err != nil {
		return nil, fmt.Errorf("failed to open file: %w", err)
	}
	buf := bytes.NewBuffer(nil)
	_, err = io.Copy(buf, fh)
	if err != nil {
		return nil, fmt.Errorf("failed to read file: %w", err)
	}
	return buf.Bytes(), nil
}
