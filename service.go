package requestmock

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net"
	"net/http"
	"net/http/httputil"
	"strconv"
)

type Service struct {
	st           *Settings
	router       *gin.Engine
	storage      Storage
	storageInput chan *storeItem
}

func NewService(st *Settings) *Service {

	router := gin.New()

	return &Service{
		st:     st,
		router: router,
	}
}

func (s *Service) Start(ctx context.Context) error {

	defer func() {
		close(s.storageInput)
	}()

	// init storage
	storage, err := storageFactory(s.st.Storage)
	if err != nil {
		return fmt.Errorf("failed to init storage: %w", err)
	}

	// start workers
	s.storageInput = make(chan *storeItem, 200)
	for i := 0; i < s.st.Storage.Workers; i++ {
		go s.startWorker()
	}

	s.storage = storage
	s.router.Use(s.storeHandler)

	for _, route := range s.st.Routes {
		s.router.Handle(route.Method, route.Path, makeHandler(route))
	}

	listener, err := net.Listen("tcp4", "0.0.0.0:"+strconv.Itoa(s.st.Port))
	if err != nil {
		return err
	}
	server := http.Server{
		Handler: s.router,
	}
	return server.Serve(listener)
}

func (s *Service) storeHandler(ctx *gin.Context) {

	// we will lose request.Body if request ends, should read it here.
	out, err := httputil.DumpRequest(ctx.Request, true)
	if err != nil {
		log.Error().Msg(err.Error())
	} else {
		s.storageInput <- &storeItem{out: string(out)}
	}

	ctx.Next()
}
