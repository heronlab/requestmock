FROM golang:1.16

WORKDIR /src
ADD . /src
RUN go mod download && CGO_ENABLED=0 go build -o app ./cmd

# --- runtime
FROM alpine:3.11

WORKDIR /app

COPY --from=0 /src/app /usr/bin/app
COPY --from=0 /src/cmd/config.yml /app

CMD ["app"]
